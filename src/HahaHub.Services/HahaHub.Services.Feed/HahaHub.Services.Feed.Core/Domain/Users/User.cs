﻿using HahaHub.Services.Feed.Core.Domain.Posts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HahaHub.Services.Feed.Core.Domain.Users
{
    public class User
     : BaseAuditableEntity, ISoftDeletable
    {
        //Имя пользователя (разделить имя на никнейм и ФИО)
        [Required]
        [StringLength(256)]
        public string Name { get; set; }

        //Электронная почта пользователя
        [StringLength(256)]
        public string Email { get; set; }

        //Пароль пользователя
        [StringLength(128)]
        public string Password { get; set; }

        public virtual IList<Post> Posts { get; set; }

        public virtual IList<Comment> Comments { get; set; }

        public virtual IList<Subscription> Subscriptions { get; set; } = new List<Subscription>();

        public bool IsDeleted { get; set; }
    }
}