﻿using System.Collections.Generic;

namespace HahaHub.Services.Feed.Core.Domain.Users
{
    public class Subscription : BaseAuditableEntity
    {
        public int UserId { get; set; }

        public virtual User User { get; set; }

        public virtual User SubscriptionUser { get; set; }
    }
}
