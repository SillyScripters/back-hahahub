﻿namespace HahaHub.Services.Feed.Core.Domain
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}