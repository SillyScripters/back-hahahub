﻿namespace HahaHub.Services.Feed.Core.Domain
{
    public interface ISoftDeletable
    {
        bool IsDeleted { get; set; }
    }
}
