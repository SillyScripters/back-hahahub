﻿using HahaHub.Services.Feed.Core.Domain.Users;

namespace HahaHub.Services.Feed.Core.Domain.Posts
{
    public class Like : BaseAuditableEntity
    {
        public int UserId { get; set; }

        public int PostId { get; set; }

        public virtual User User { get; set; }

        public virtual Post Post { get; set; }
    }
}
