﻿using System;
using System.Collections;
using System.Collections.Generic;
using HahaHub.Services.Feed.Core.Domain.Users;

namespace HahaHub.Services.Feed.Core.Domain.Posts
{
    public class Post
        : BaseAuditableEntity, ISoftDeletable
    {
        public string PostText { get; set; }

        public string ImageURL { get; set; }

        public string HashTag { get; set; }

        public int UserId { get; set; }

        public int CategoryId { get; set; }

        public virtual User User { get; set; }

        public virtual Category Category { get; set; }

        public virtual IList<Like> Likes { get; set; }

        public virtual IList<Comment> Comments { get; set; }

        public bool IsDeleted { get; set; }
    }
}