﻿using System;
using System.Collections.Generic;

namespace HahaHub.Services.Feed.Core.Domain.Posts
{
    public class Category
        :BaseAuditableEntity, ISoftDeletable
    {
        public string Name { get; set; }

        public virtual IList<Post> Posts { get; set; }

        public bool IsDeleted { get; set; }
    }
}