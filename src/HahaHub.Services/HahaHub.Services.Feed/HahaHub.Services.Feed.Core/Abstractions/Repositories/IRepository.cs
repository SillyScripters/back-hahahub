﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using HahaHub.Services.Feed.Core.Domain;

namespace HahaHub.Services.Feed.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        IQueryable<T> GetAll();

        Task<IEnumerable<T>> GetAllAsync(CancellationToken cancellationToken);

        Task<IEnumerable<T>> GetPagedAsync(int page, int pageSize, CancellationToken cancellationToken); 

        Task<T> GetByIdAsync(int id, CancellationToken cancellationToken);
        
        Task<IEnumerable<T>> GetRangeByIdsAsync(List<int> ids, CancellationToken cancellationToken);

        Task<int> AddAsync(T entity, CancellationToken cancellationToken);

        Task UpdateAsync(T entity, CancellationToken cancellationToken);

        Task DeleteAsync(T entity, CancellationToken cancellationToken);

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}