﻿using HahaHub.Services.Feed.WebHost.Infrastructure;
using Microsoft.Extensions.DependencyInjection;

namespace HahaHub.Services.Feed.WebHost
{
    public static class WebExtensions
    {
        public static IServiceCollection AddWebServices(this IServiceCollection services)
        {
            services.AddControllers().AddMvcOptions(x =>
                x.SuppressAsyncSuffixInActionNames = false);
            services.AddOpenApiDocument(options =>
            {
                options.Title = "Haha Hub API Doc";
                options.Version = "1.0";
            });
            services.AddExceptionHandler<CustomExceptionHandler>();
            services.AddHttpLogging(o => { });
            return services;
        }
    }
}
