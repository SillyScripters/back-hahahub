﻿namespace HahaHub.Services.Feed.WebHost.Models
{
    public class CategoryIdsFilter
    {
        public int[] CategoryIds { get; set; }
    }
}
