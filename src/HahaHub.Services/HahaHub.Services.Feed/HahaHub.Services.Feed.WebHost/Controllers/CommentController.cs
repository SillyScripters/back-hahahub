﻿using HahaHub.Services.Feed.Services.Abstractions;
using HahaHub.Services.Feed.Services.Contracts.Comments;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;

namespace HahaHub.Services.Feed.WebHost.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly ICommentService _commentService;

        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        /// <summary>
        /// Получить комментарий по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<CommentDto> Get(int id, CancellationToken cancellationToken)
        {
            return await _commentService.GetByIdAsync(id, cancellationToken);
        }

        /// <summary>
        /// Добавить комментарий
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post(CreatingCommentDto request, CancellationToken cancellationToken)
        {
            return Ok(await _commentService.AddAsync(request, cancellationToken));
        }

        /// <summary>
        /// Отредактировать комментарий
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, UpdatingCommentDto request, CancellationToken cancellationToken)
        {
            await _commentService.UpdateAsync(id, request, cancellationToken);
            return Ok();
        }

        /// <summary>
        /// Удалить комментарий
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id, CancellationToken cancellationToken)
        {
            await _commentService.DeleteAsync(id, cancellationToken);
            return Ok();
        }
    }
}
