﻿using HahaHub.Services.Feed.Services.Abstractions;
using HahaHub.Services.Feed.Services.Contracts.Likes;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;

namespace HahaHub.Services.Feed.WebHost.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class LikeController : ControllerBase
    {
        private readonly ILikeService _likeService;

        public LikeController(ILikeService likeService)
        {
            _likeService = likeService;
        }

        /// <summary>
        /// Добавить лайк
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post(CreatingOrDeletingLikeDto request, CancellationToken cancellationToken)
        {
            return Ok(await _likeService.AddAsync(request, cancellationToken));
        }

        /// <summary>
        /// Удалить лайк
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> Delete(CreatingOrDeletingLikeDto request, CancellationToken cancellationToken)
        {
            await _likeService.DeleteAsync(request, cancellationToken);
            return Ok();
        }
    }
}
