﻿using HahaHub.Services.Feed.Services.Abstractions;
using HahaHub.Services.Feed.Services.Contracts.Posts;
using HahaHub.Services.Feed.WebHost.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;


namespace HahaHub.Services.Feed.WebHost.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PostController : ControllerBase
    {
        private readonly IPostService _postService;
        private readonly ICommentService _commentService;

        public PostController(IPostService postService, ICommentService commentService)
        {
            _postService = postService;
            _commentService = commentService;
        }

        /// <summary>
        /// Получить пост по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id, CancellationToken cancellationToken)
        {
            return Ok(await _postService.GetByIdAsync(id, cancellationToken));
        }

        /// <summary>
        /// Получить постраничный список постов (с фильтром категорий)
        /// </summary>
        /// <param name="request"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpPost("page")]
        public async Task<IActionResult> GetPaginatedList(
            CategoryIdsFilter request,
            int pageNumber,
            int pageSize,
            CancellationToken cancellationToken)
        {
            return Ok(await _postService.GetPaginatedListAsync(
                request.CategoryIds,
                pageNumber,
                pageSize,
                cancellationToken));
        }

        /// <summary>
        /// Получить постраничный список постов по подпискам (с фильтром категорий)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="request"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpPost("subscriptions-page")]
        public async Task<IActionResult> GetPaginatedListBySubscriptions(
            int userId,
            CategoryIdsFilter request,
            int pageNumber,
            int pageSize,
            CancellationToken cancellationToken)
        {
            return Ok(await _postService.GetPaginatedListBySubscriptionsAsync(
                userId,
                request.CategoryIds,
                pageNumber,
                pageSize,
                cancellationToken));
        }

        /// <summary>
        /// Получить постраничный список комментариев поста
        /// </summary>
        /// <param name="postId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpPost("{postId}/comments")]
        public async Task<IActionResult> GetPostCommentsPaginatedList(
            int postId,
            int pageNumber,
            int pageSize,
            CancellationToken cancellationToken
            )
        {
            return Ok(await _commentService.GetPaginatedListByPostIdAsync(
                postId,
                pageNumber,
                pageSize,
                cancellationToken));
        }

        /// <summary>
        /// Добавить пост
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post(CreatingPostDto request, CancellationToken cancellationToken)
        {
            return Ok(await _postService.AddAsync(request, cancellationToken));
        }

        /// <summary>
        /// Отредактировать пост
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, UpdatingPostDto request, CancellationToken cancellationToken)
        {
            await _postService.UpdateAsync(id, request, cancellationToken);
            return Ok();
        }

        /// <summary>
        /// Удалить пост
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id, CancellationToken cancellationToken)
        {
            await _postService.DeleteAsync(id, cancellationToken);
            return Ok();
        }
    }
}