﻿using HahaHub.Services.Feed.Services.Abstractions;
using HahaHub.Services.Feed.Services.Contracts.Categories;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;


namespace HahaHub.Services.Feed.WebHost.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        /// <summary>
        /// Получить категорию по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns> ДТО поста </returns>

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id, CancellationToken cancellationToken)
        {
            return Ok(await _categoryService.GetByIdAsync(id, cancellationToken));
        }

        /// <summary>
        /// Получить постраничный список
        /// </summary>
        /// <param name="pageNumber"> номер страницы </param>
        /// <param name="pageSize"> количество элементов на странице </param>
        /// <param name="cancellationToken"></param>
        /// <returns> Список ДТО постов </returns>
        [HttpPost("page")]
        public async Task<IActionResult> GetPaginatedList(
            int pageNumber,
            int pageSize,
            CancellationToken cancellationToken
            )
        {
            return Ok(await _categoryService.GetPaginatedListAsync(
                pageNumber, 
                pageSize, 
                cancellationToken));
        }

        /// <summary>
        /// Добавить категорию
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post(CreatingCategoryDto request, CancellationToken cancellationToken)
        {
            return Ok(await _categoryService.AddAsync(request, cancellationToken));
        }

        /// <summary>
        /// Редактирование категорий не требуется
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> Update()
        {
            return Ok();
        }

        /// <summary>
        /// Удалить категорию по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id, CancellationToken cancellationToken)
        {
            await _categoryService.DeleteAsync(id, cancellationToken);
            return Ok();
        }


    }
}