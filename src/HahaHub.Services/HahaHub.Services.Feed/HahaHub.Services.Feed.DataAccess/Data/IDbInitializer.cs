﻿namespace HahaHub.Services.Feed.DataAccess.Data
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}