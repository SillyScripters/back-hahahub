﻿using HahaHub.Services.Feed.Core.Domain.Users;

namespace HahaHub.Services.Feed.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        
        public void InitializeDb()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            _dataContext.AddRange(FakeDataFactory.Users);
            _dataContext.SaveChanges();

            var user1 = _dataContext.Users.Find(-1);
            var user2 = _dataContext.Users.Find(-2);
            var user3 = _dataContext.Users.Find(-3);
            user1.Subscriptions.Add(new Subscription { Id = user2.Id });
            user1.Subscriptions.Add(new Subscription { Id = user3.Id });
            user2.Subscriptions.Add(new Subscription { Id = user1.Id });
            _dataContext.SaveChanges();

            _dataContext.AddRange(FakeDataFactory.Categories);
            _dataContext.SaveChanges();

            _dataContext.AddRange(FakeDataFactory.Posts);
            _dataContext.SaveChanges();

            _dataContext.AddRange(FakeDataFactory.Comments);
            _dataContext.SaveChanges();

            _dataContext.AddRange(FakeDataFactory.Likes);
            _dataContext.SaveChanges();
        }
    }
}