﻿using System;
using System.Collections.Generic;
using System.Linq;
using HahaHub.Services.Feed.Core.Domain.Users;
using HahaHub.Services.Feed.Core.Domain.Posts;
using System.Net.NetworkInformation;

namespace HahaHub.Services.Feed.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static List<User> Users => new List<User>()
        {

            new User()
            {
                Id = -1,
                Email = "owner@somemail.ru",
                Name = "Иван",
                Password ="111"
            },
            new User()
             {
                Id = -2,
                Email = "andreev@somemail.ru",
                Name = "Петр",
                Password ="111"
            },
            new User()
            {
                Id = -3,
                Email = "vasev@somemail.ru",
                Name = "Вася",
                Password ="111"
            }
        };

        public static List<Category> Categories => new List<Category>()
        {

            new Category()
            {
                Id = -1,
                Name = "Ирония",
            },
            new Category()
            {
                Id = -2,
                Name = "Неожиданная развязка",
            },
            new Category()
            {
                Id = -3,
                Name = "Одесские анекдоты",
            }
        };

        public static List<Post> Posts => new List<Post>()
                {
                    new Post()
                    {
                        Id = -1,
                        PostText = "По моей фигуре понятно, что я поклонница хлеба, а не зрелищ.",
                        ImageURL = "",
                        HashTag = "#фигура",
                        CategoryId = -1,
                        UserId = -1
                    }
        };

        public static List<Comment> Comments => new List<Comment>()
        {
            new Comment()
            {
                Id = -1,
                CommentText = "Ору",
                UserId = -2,
                PostId = -1
            },
            new Comment()
            {
                Id = -2,
                CommentText = "xD",
                UserId = -1,
                PostId = -1
            },
            new Comment()
            {
                Id = -3,
                CommentText = "Жиза",
                UserId = -2,
                PostId = -1
            },
            new Comment()
            {
                Id = -4,
                CommentText = "Спасибо",
                UserId = -1,
                PostId = -1
            },
        };

        public static List<Like> Likes => new List<Like>()
        {
            new Like()
            {
                Id = -1,
                UserId = -1,
                PostId = -1
            },
            new Like()
            {
                Id = -2,
                UserId = -2,
                PostId = -1
            }
        };
    }
}