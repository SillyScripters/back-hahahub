﻿using HahaHub.Services.Feed.Core.Domain;
using HahaHub.Services.Feed.Core.Domain.Posts;
using HahaHub.Services.Feed.Core.Domain.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Linq.Expressions;
using System.Reflection;

namespace HahaHub.Services.Feed.DataAccess
{
    public class DataContext
        : DbContext
    {
        public DbSet<Post> Posts { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Subscription> Subscriptions { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Like> Likes { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DataContext()
        {

        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Добавляем фильтры IsDeleted == false для ISoftDeletable
            foreach (var entityType in modelBuilder.Model.GetEntityTypes())
            {
                if (typeof(ISoftDeletable).IsAssignableFrom(entityType.ClrType))
                {
                    entityType.AddSoftDeleteQueryFilter();
                }
            }

            modelBuilder.Entity<Post>()
                            .HasOne(r => r.Category)
                            .WithMany(p => p.Posts);

            modelBuilder.Entity<Post>()
                            .HasOne(r => r.User)
                            .WithMany(p => p.Posts);

            modelBuilder.Entity<Like>()
                            .HasIndex(l => new { l.UserId, l.PostId }).IsUnique();

            modelBuilder.Entity<Like>()
                            .HasOne(l => l.Post)
                            .WithMany(p => p.Likes)
                            .IsRequired();

            modelBuilder.Entity<Comment>()
                            .HasOne(c => c.Post)
                            .WithMany(p => p.Comments)
                            .IsRequired();

            modelBuilder.Entity<Comment>()
                            .HasOne(c => c.User)
                            .WithMany(p => p.Comments)
                            .IsRequired();

            modelBuilder.Entity<Category>()
                            .HasIndex(c => c.Name).IsUnique();

            modelBuilder.Entity<Subscription>().HasKey(s => new { s.Id, s.UserId });
            modelBuilder.Entity<Subscription>()
                            .HasOne(s => s.User)
                            .WithMany(u => u.Subscriptions)
                            .HasForeignKey(s => s.UserId);
            modelBuilder.Entity<Subscription>()
                            .HasOne(s => s.SubscriptionUser)
                            .WithMany()
                            .HasForeignKey(s => s.Id);
        }
    }

    /// <summary>
    /// Расширение для автоматического добавления фильтров IsDeleted == false
    /// </summary>
    public static class SoftDeleteQueryExtension
    {
        public static void AddSoftDeleteQueryFilter(
            this IMutableEntityType entityData)
        {
            var methodToCall = typeof(SoftDeleteQueryExtension)
                .GetMethod(nameof(GetSoftDeleteFilter),
                    BindingFlags.NonPublic | BindingFlags.Static)
                .MakeGenericMethod(entityData.ClrType);
            var filter = methodToCall.Invoke(null, new object[] { });
            entityData.SetQueryFilter((LambdaExpression)filter);
            entityData.AddIndex(entityData.
                 FindProperty(nameof(ISoftDeletable.IsDeleted)));
        }

        private static LambdaExpression GetSoftDeleteFilter<TEntity>()
            where TEntity : class, ISoftDeletable
        {
            Expression<Func<TEntity, bool>> filter = x => !x.IsDeleted;
            return filter;
        }
    }
}