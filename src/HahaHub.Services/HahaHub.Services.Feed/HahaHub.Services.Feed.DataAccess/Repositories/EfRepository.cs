﻿using HahaHub.Services.Feed.Core.Abstractions.Repositories;
using HahaHub.Services.Feed.Core.Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HahaHub.Services.Feed.DataAccess.Repositories
{
    public class EfRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        private readonly DataContext _dataContext;

        public EfRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IQueryable<T> GetAll()
        {
            return _dataContext.Set<T>();
        }
        
        public async Task<IEnumerable<T>> GetAllAsync(CancellationToken cancellationToken)
        {
            var entities = await _dataContext.Set<T>().ToListAsync(cancellationToken);

            return entities;
        }
        public async Task<IEnumerable<T>> GetPagedAsync(int page, int pageSize, CancellationToken cancellationToken)
        {
            var entities = await _dataContext.Set<T>()
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync(cancellationToken);

            return entities;
        }
        public async Task<T> GetByIdAsync(int id, CancellationToken cancellationToken)
        {
            var entity = await _dataContext.Set<T>().FindAsync(new object[] { id }, cancellationToken);

            return entity;
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<int> ids, CancellationToken cancellationToken)
        {
            var entities = await _dataContext.Set<T>().Where(x => ids.Contains(x.Id)).ToListAsync(cancellationToken);
            
            return entities;
        }

        public async Task<int> AddAsync(T entity, CancellationToken cancellationToken)
        {
            var result = await _dataContext.Set<T>().AddAsync(entity);

            await _dataContext.SaveChangesAsync();

            return result.Entity.Id;
        }

        public async Task UpdateAsync(T entity, CancellationToken cancellationToken)
        {
            _dataContext.Set<T>().Update(entity);

            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(T entity, CancellationToken cancellationToken)
        {
            _dataContext.Set<T>().Remove(entity);

            await _dataContext.SaveChangesAsync();
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return await _dataContext.SaveChangesAsync();
        }
    }
}