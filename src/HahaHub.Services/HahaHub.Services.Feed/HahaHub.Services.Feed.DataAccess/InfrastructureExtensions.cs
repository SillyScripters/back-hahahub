﻿using HahaHub.Services.Feed.Infrastructure.Data.Interceptors;
using HahaHub.Services.Feed.Core.Abstractions.Repositories;
using HahaHub.Services.Feed.DataAccess.Data;
using HahaHub.Services.Feed.DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
//using EntityFramework.Exceptions.Sqlite;
using EntityFramework.Exceptions.PostgreSQL;

namespace HahaHub.Services.Feed.DataAccess
{
    public static class InfrastructureExtensions
    {
        public static IServiceCollection AddInfrastructureServices(this IServiceCollection services, IConfiguration configuration)
        {
            //var sqliteConnectionString = configuration.GetConnectionString("SqliteDb");
            var connectionString = configuration.GetConnectionString("PgsqlDb");

            services.AddSingleton(TimeProvider.System);
            services.AddScoped<ISaveChangesInterceptor, AuditableEntityInterceptor>();
            services.AddDbContext<DataContext>((sp, options) =>
            {
                options.AddInterceptors(sp.GetServices<ISaveChangesInterceptor>())
                    //.UseSqlite(sqliteConnectionString)
                    .UseNpgsql(connectionString)
                    .UseSnakeCaseNamingConvention()
                    .UseLazyLoadingProxies()
                    .UseExceptionProcessor();
            });
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<IDbInitializer, EfDbInitializer>();
            return services;
        }
    }
}
