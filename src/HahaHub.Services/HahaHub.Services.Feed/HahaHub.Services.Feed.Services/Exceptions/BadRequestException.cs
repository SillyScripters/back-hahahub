﻿using Ardalis.GuardClauses;
using HahaHub.Services.Feed.Services.Helpers;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;

namespace HahaHub.Services.Feed.Services.Exceptions
{
    public class BadRequestException : Exception
    {
        public BadRequestException(string message) : base(message) { }

        public BadRequestException(string key, string message)
            : base($"{message}. Key: {key}") { }

        public static void ThrowUniqueConstraintException<T>(
            object key,
            [NotNull][ValidatedNotNull] T input,
            [CallerArgumentExpression("input")] string parameterName = null)
        {
            throw new BadRequestException(
                CompositeKeyHelper.GetCompositeKey(key),
                $"Queried object {parameterName} already exists");
        }

        public static void ThrowForeignConstraintException<T>(
            object key,
            [NotNull][ValidatedNotNull] T input,
            [CallerArgumentExpression("input")] string parameterName = null)
        {
            throw new BadRequestException(
                CompositeKeyHelper.GetCompositeKey(key),
                $"Queried object {parameterName} violates foreign key constraint");
        }
    }
}
