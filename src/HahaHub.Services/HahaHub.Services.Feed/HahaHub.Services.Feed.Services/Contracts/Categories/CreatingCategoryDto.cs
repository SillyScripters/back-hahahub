﻿namespace HahaHub.Services.Feed.Services.Contracts.Categories
{
    public class CreatingCategoryDto
    {
        public string Name { get; set; }
    }
}
