﻿using AutoMapper;
using HahaHub.Services.Feed.Core.Domain.Posts;

namespace HahaHub.Services.Feed.Services.Contracts.Categories
{
    public class CategoryDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        private class Mapping : Profile
        {
            public Mapping()
            {
                CreateMap<Category, CategoryDto>();
            }
        }
    }
}
