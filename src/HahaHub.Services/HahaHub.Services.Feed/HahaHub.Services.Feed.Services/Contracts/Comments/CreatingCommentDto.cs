﻿namespace HahaHub.Services.Feed.Services.Contracts.Comments
{
    public class CreatingCommentDto
    {
        public int UserId { get; set; }

        public int PostId { get; set; }

        public string CommentText {  get; set; }
    }
}
