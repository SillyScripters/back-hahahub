﻿using AutoMapper;
using HahaHub.Services.Feed.Core.Domain.Posts;
using HahaHub.Services.Feed.Services.Contracts.Users;
using System;

namespace HahaHub.Services.Feed.Services.Contracts.Comments
{
    public class CommentDto
    {
        public int Id { get; set; }

        public string CommentText { get; set; }

        public UserDto User { get; set; }

        public DateTimeOffset Created {  get; set; }

        public DateTimeOffset LastModified { get; set; }

        private class Mapping : Profile
        {
            public Mapping()
            {
                CreateMap<Comment, CommentDto>()
                    .ForMember(d => d.User, map => map.MapFrom(src => src.User));
            }
        }
    }
}
