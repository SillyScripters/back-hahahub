﻿namespace HahaHub.Services.Feed.Services.Contracts.Comments
{
    public class UpdatingCommentDto
    {
        public string CommentText {  get; set; }
    }
}
