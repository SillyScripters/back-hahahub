﻿using AutoMapper;
using HahaHub.Services.Feed.Core.Domain.Users;

namespace HahaHub.Services.Feed.Services.Contracts.Users
{
    public class UserDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        private class Mapping : Profile
        {
            public Mapping()
            {
                CreateMap<User, UserDto>();
            }
        }
    }
}
