﻿namespace HahaHub.Services.Feed.Services.Contracts.Likes
{
    public class CreatingOrDeletingLikeDto
    {
        public int UserId { get; set; }

        public int PostId { get; set; }
    }
}
