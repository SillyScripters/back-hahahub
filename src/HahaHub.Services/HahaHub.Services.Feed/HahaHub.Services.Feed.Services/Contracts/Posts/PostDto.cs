﻿using AutoMapper;
using HahaHub.Services.Feed.Core.Domain.Posts;
using HahaHub.Services.Feed.Services.Contracts.Categories;
using HahaHub.Services.Feed.Services.Contracts.Users;
using System;

namespace HahaHub.Services.Feed.Services.Contracts.Posts
{
    public class PostDto
    {
        public int Id { get; set; }

        public string PostText { get; set; }

        public string ImageURL { get; set; }

        public string HashTag { get; set; }

        public UserDto User { get; set; }

        public CategoryDto Category { get; set; }

        public DateTimeOffset Created { get; set; }

        public DateTimeOffset LastModified { get; set;}

        private class Mapping : Profile
        {
            public Mapping()
            {
                CreateMap<Post, PostDto>();
            }
        }
    }
}
