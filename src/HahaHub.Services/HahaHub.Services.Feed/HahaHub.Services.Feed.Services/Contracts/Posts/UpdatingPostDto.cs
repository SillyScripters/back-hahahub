﻿namespace HahaHub.Services.Feed.Services.Contracts.Posts
{
    public class UpdatingPostDto
    {
        public string PostText { get; set; }

        public string ImageURL { get; set; }

        public string HashTag { get; set; }

        public int? CategoryId { get; set; }
    }
}
