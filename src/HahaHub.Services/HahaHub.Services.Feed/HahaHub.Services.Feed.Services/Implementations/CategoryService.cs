﻿using Ardalis.GuardClauses;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using EntityFramework.Exceptions.Common;
using HahaHub.Services.Feed.Core.Abstractions.Repositories;
using HahaHub.Services.Feed.Core.Domain.Posts;
using HahaHub.Services.Feed.Services.Abstractions;
using HahaHub.Services.Feed.Services.Contracts;
using HahaHub.Services.Feed.Services.Contracts.Categories;
using HahaHub.Services.Feed.Services.Exceptions;
using HahaHub.Services.Feed.Services.Implementations.Mapping;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HahaHub.Services.Feed.Services.Implementations
{
    public class CategoryService : ICategoryService
    {
        private readonly IRepository<Category> _categoryRepository;
        private readonly IMapper _mapper;

        public CategoryService(IRepository<Category> categoryRepository, IMapper mapper)
        {
            _categoryRepository = categoryRepository;
            _mapper = mapper;
        }

        public async Task<CategoryDto> GetByIdAsync(int id, CancellationToken cancellationToken)
        {
            var category = await _categoryRepository.GetByIdAsync(id, cancellationToken);
            Guard.Against.NotFound(id, category);

            return new CategoryDto
            {
                Id = category.Id,
                Name = category.Name,
            };
        }

        public async Task<PaginatedList<CategoryDto>> GetPaginatedListAsync(int pageNumber, int pageSize, CancellationToken cancellationToken)
        {
            return await _categoryRepository.GetAll()
                .OrderBy(c => c.Id)
                .ProjectTo<CategoryDto>(_mapper.ConfigurationProvider)
                .PaginatedListAsync(pageNumber, pageSize, cancellationToken);
        }

        public async Task<int> AddAsync(CreatingCategoryDto categoryDto, CancellationToken cancellationToken)
        {
            Category category = new Category
            {
                Name = categoryDto.Name
            };

            int id;
            try
            {
                id = await _categoryRepository.AddAsync(category, cancellationToken);
            }
            catch (UniqueConstraintException)
            {
                BadRequestException.ThrowUniqueConstraintException(
                    new { categoryDto.Name },
                    category);
                throw;
            }
            return id;
        }

        public async Task DeleteAsync(int id, CancellationToken cancellationToken)
        {
            var category = await _categoryRepository.GetByIdAsync(id, cancellationToken);
            Guard.Against.NotFound(id, category);

            await _categoryRepository.DeleteAsync(category, cancellationToken);
            return;
        }
    }
}
