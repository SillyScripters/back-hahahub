﻿using Ardalis.GuardClauses;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using EntityFramework.Exceptions.Common;
using HahaHub.Services.Feed.Core.Abstractions.Repositories;
using HahaHub.Services.Feed.Core.Domain.Posts;
using HahaHub.Services.Feed.Services.Abstractions;
using HahaHub.Services.Feed.Services.Contracts;
using HahaHub.Services.Feed.Services.Contracts.Comments;
using HahaHub.Services.Feed.Services.Contracts.Users;
using HahaHub.Services.Feed.Services.Exceptions;
using HahaHub.Services.Feed.Services.Implementations.Mapping;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HahaHub.Services.Feed.Services.Implementations
{
    public class CommentService : ICommentService
    {
        private readonly IRepository<Comment> _commentRepository;
        private readonly IMapper _mapper;

        public CommentService(IRepository<Comment> commentRepository, IMapper mapper)
        {
            _commentRepository = commentRepository;
            _mapper = mapper;
        }

        public async Task<CommentDto> GetByIdAsync(int id, CancellationToken cancellationToken)
        {
            var comment = await _commentRepository.GetByIdAsync(id, cancellationToken);
            Guard.Against.NotFound(id, comment);

            return new CommentDto
            {
                Id = comment.Id,
                CommentText = comment.CommentText,
                User = new UserDto
                {
                    Id = comment.User.Id,
                    Name = comment.User.Name
                },
                Created = comment.Created,
                LastModified = comment.LastModified
            };
        }

        public async Task<PaginatedList<CommentDto>> GetPaginatedListByPostIdAsync(int postId, int pageNumber, int pageSize, CancellationToken cancellationToken)
        {
            return await _commentRepository.GetAll()
                .Where(c => c.PostId == postId)
                .OrderBy(c => c.Id)
                .ProjectTo<CommentDto>(_mapper.ConfigurationProvider)
                .PaginatedListAsync(pageNumber, pageSize, cancellationToken);
        }

        public async Task<int> AddAsync(CreatingCommentDto commentDto, CancellationToken cancellationToken)
        {
            Comment comment = new Comment()
            {
                UserId = commentDto.UserId,
                PostId = commentDto.PostId,
                CommentText = commentDto.CommentText
            };

            int id;
            try
            {
                id = await _commentRepository.AddAsync(comment, cancellationToken);
            }
            catch (ReferenceConstraintException)
            {
                BadRequestException.ThrowForeignConstraintException(
                    new { commentDto.UserId, commentDto.PostId },
                    comment);
                throw;
            }
            return id;
        }

        public async Task DeleteAsync(int id, CancellationToken cancellationToken)
        {
            var comment = await _commentRepository.GetByIdAsync(id, cancellationToken);
            Guard.Against.NotFound(id, comment);

            await _commentRepository.DeleteAsync(comment, cancellationToken);
        }

        public async Task UpdateAsync(int id, UpdatingCommentDto commentDto, CancellationToken cancellationToken)
        {
            var comment = await _commentRepository.GetByIdAsync(id, cancellationToken);
            Guard.Against.NotFound(id, comment);

            comment.CommentText = commentDto.CommentText;
            await _commentRepository.SaveChangesAsync(cancellationToken);            
        }
    }
}
