﻿using Ardalis.GuardClauses;
using EntityFramework.Exceptions.Common;
using HahaHub.Services.Feed.Core.Abstractions.Repositories;
using HahaHub.Services.Feed.Core.Domain.Posts;
using HahaHub.Services.Feed.Services.Abstractions;
using HahaHub.Services.Feed.Services.Contracts.Likes;
using HahaHub.Services.Feed.Services.Exceptions;
using HahaHub.Services.Feed.Services.Helpers;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HahaHub.Services.Feed.Services.Implementations
{
    public class LikeService : ILikeService
    {
        private readonly IRepository<Like> _likeRepository;

        public LikeService(IRepository<Like> likeRepository)
        {
            _likeRepository = likeRepository;
        }

        public async Task<int> AddAsync(CreatingOrDeletingLikeDto likeDto, CancellationToken cancellationToken)
        {
            Like like = new Like()
            {
                UserId = likeDto.UserId,
                PostId = likeDto.PostId,
            };

            int id;
            try
            {
                id = await _likeRepository.AddAsync(like, cancellationToken);
            }
            catch (ReferenceConstraintException)
            {
                BadRequestException.ThrowForeignConstraintException(
                    new { likeDto.UserId, likeDto.PostId },
                    like);
                throw;
            }
            catch (UniqueConstraintException)
            {
                BadRequestException.ThrowUniqueConstraintException(
                    new { likeDto.UserId, likeDto.PostId },
                    like);
                throw;
            }
            return id;
            }

            public async Task DeleteAsync(CreatingOrDeletingLikeDto likeDto, CancellationToken cancellationToken)
            {
                var like = await _likeRepository.GetAll()
                    .Where(l => l.UserId == likeDto.UserId && l.PostId == likeDto.PostId)
                    .SingleOrDefaultAsync();
                Guard.Against.NotFound(CompositeKeyHelper.GetCompositeKey(new { likeDto.UserId, likeDto.PostId }), like);

                await _likeRepository.DeleteAsync(like, cancellationToken);
            }
        }
    }
