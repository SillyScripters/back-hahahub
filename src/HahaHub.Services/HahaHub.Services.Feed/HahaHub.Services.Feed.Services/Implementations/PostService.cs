﻿using Ardalis.GuardClauses;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using EntityFramework.Exceptions.Common;
using HahaHub.Services.Feed.Core.Abstractions.Repositories;
using HahaHub.Services.Feed.Core.Domain.Posts;
using HahaHub.Services.Feed.Core.Domain.Users;
using HahaHub.Services.Feed.Services.Abstractions;
using HahaHub.Services.Feed.Services.Contracts;
using HahaHub.Services.Feed.Services.Contracts.Categories;
using HahaHub.Services.Feed.Services.Contracts.Posts;
using HahaHub.Services.Feed.Services.Contracts.Users;
using HahaHub.Services.Feed.Services.Exceptions;
using HahaHub.Services.Feed.Services.Implementations.Mapping;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HahaHub.Services.Feed.Services.Implementations
{
    public class PostService : IPostService
    {
        private readonly IRepository<Post> _postRepository;
        private readonly IRepository<Subscription> _subscriptionRepository;
        private readonly IMapper _mapper;

        public PostService(IRepository<Post> postRepository, IRepository<Subscription> subscriptionRepository, IMapper mapper)
        {
            _postRepository = postRepository;
            _subscriptionRepository = subscriptionRepository;
            _mapper = mapper;
        }

        public async Task<PostDto> GetByIdAsync(int id, CancellationToken cancellationToken)
        {
            var comment = await _postRepository.GetByIdAsync(id, cancellationToken);
            Guard.Against.NotFound(id, comment);

            return new PostDto
            {
                Id = comment.Id,
                PostText = comment.PostText,
                ImageURL = comment.ImageURL,
                HashTag = comment.HashTag,
                User = new UserDto
                {
                    Id = comment.User.Id,
                    Name = comment.User.Name,
                },
                Category = new CategoryDto
                {
                    Id = comment.Category.Id,
                    Name = comment.Category.Name,
                },
                Created = comment.Created,
                LastModified = comment.LastModified
            };
        }

        public async Task<PaginatedList<PostDto>> GetPaginatedListAsync(
            int[] categoryIds,
            int pageNumber,
            int pageSize,
            CancellationToken cancellationToken)
        {
            return await _postRepository.GetAll()
                .OrderByDescending(p => p.Id)
                .Where(p => categoryIds == null || categoryIds.Contains(p.Id))
                .ProjectTo<PostDto>(_mapper.ConfigurationProvider)
                .PaginatedListAsync(pageNumber, pageSize, cancellationToken);
        }

        public async Task<PaginatedList<PostDto>> GetPaginatedListBySubscriptionsAsync(
            int userId,
            int[] categoryIds,
            int pageNumber,
            int pageSize,
            CancellationToken cancellationToken)
        {
            var subscriptionsQuery = _subscriptionRepository.GetAll().Where(s => s.UserId == userId).Select(s => s.Id);

            return await _postRepository.GetAll()
                .OrderByDescending(p => p.Id)
                .Where(p => categoryIds == null || categoryIds.Contains(p.Id))
                .Where(p => subscriptionsQuery.Contains(p.UserId))
                .ProjectTo<PostDto>(_mapper.ConfigurationProvider)
                .PaginatedListAsync(pageNumber, pageSize, cancellationToken);
        }

        public async Task<int> AddAsync(CreatingPostDto postDto, CancellationToken cancellationToken)
        {
            Post post = new Post
            {
                UserId = postDto.UserId,
                PostText = postDto.PostText,
                ImageURL = postDto.ImageURL,
                HashTag = postDto.HashTag,
                CategoryId = postDto.CategoryId
            };


            int id;
            try
            {
                id = await _postRepository.AddAsync(post, cancellationToken);
            }
            catch (ReferenceConstraintException)
            {
                BadRequestException.ThrowForeignConstraintException(
                    new { postDto.UserId, postDto.CategoryId },
                    post);
                throw;
            }
            return id;
        }

        public async Task UpdateAsync(int id, UpdatingPostDto postDto, CancellationToken cancellationToken)
        {
            var post = await _postRepository.GetByIdAsync(id, cancellationToken);
            Guard.Against.NotFound(id, post);

            post.PostText = postDto.PostText;
            post.ImageURL = postDto.ImageURL;
            post.HashTag = postDto.HashTag;
            if (postDto.CategoryId.HasValue)
                post.CategoryId = postDto.CategoryId.Value;

            try
            {
                await _postRepository.SaveChangesAsync(cancellationToken);
            }
            catch (ReferenceConstraintException)
            {
                BadRequestException.ThrowForeignConstraintException(
                    new { postDto.CategoryId },
                    post);
                throw;
            }
            return;
        }

        public async Task DeleteAsync(int id, CancellationToken cancellationToken)
        {
            var post = await _postRepository.GetByIdAsync(id, cancellationToken);
            Guard.Against.NotFound(id, post);

            await _postRepository.DeleteAsync(post, cancellationToken);
            return;
        }
    }
}
