﻿using System.Reflection;

namespace HahaHub.Services.Feed.Services.Helpers
{
    public static class CompositeKeyHelper
    {
        public static string GetCompositeKey(object key)
        {
            string compositeKey = string.Empty;
            foreach (PropertyInfo property in key.GetType().GetProperties())
            {
                compositeKey += $"{property.Name}_{property.GetValue(key)}_";
            }
            return compositeKey.TrimEnd('_');
        }
    }
}
