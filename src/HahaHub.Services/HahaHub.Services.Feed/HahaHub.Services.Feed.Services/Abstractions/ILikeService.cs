﻿using HahaHub.Services.Feed.Services.Contracts.Likes;
using System.Threading;
using System.Threading.Tasks;

namespace HahaHub.Services.Feed.Services.Abstractions
{
    public interface ILikeService
    {
        Task<int> AddAsync(CreatingOrDeletingLikeDto likeDto, CancellationToken cancellationToken);

        Task DeleteAsync(CreatingOrDeletingLikeDto likeDto, CancellationToken cancellationToken);
    }
}
