﻿using HahaHub.Services.Feed.Services.Contracts;
using HahaHub.Services.Feed.Services.Contracts.Posts;
using System.Threading;
using System.Threading.Tasks;

namespace HahaHub.Services.Feed.Services.Abstractions
{
    public interface IPostService
    {
        Task<PostDto> GetByIdAsync(int id, CancellationToken cancellationToken);

        Task<PaginatedList<PostDto>> GetPaginatedListAsync(
            int[] categoryIds, 
            int pageNumber, 
            int pageSize, 
            CancellationToken cancellationToken);

        Task<PaginatedList<PostDto>> GetPaginatedListBySubscriptionsAsync(
            int userId,
            int[] categoryIds,
            int pageNumber,
            int pageSize,
            CancellationToken cancellationToken);

        Task<int> AddAsync(CreatingPostDto postDto, CancellationToken cancellationToken);

        Task UpdateAsync(int id, UpdatingPostDto postDto, CancellationToken cancellationToken);

        Task DeleteAsync(int id, CancellationToken cancellationToken);
    }
}
