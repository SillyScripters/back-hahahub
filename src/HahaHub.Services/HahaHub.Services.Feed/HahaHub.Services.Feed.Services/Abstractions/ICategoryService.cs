﻿using HahaHub.Services.Feed.Services.Contracts;
using HahaHub.Services.Feed.Services.Contracts.Categories;
using System.Threading;
using System.Threading.Tasks;

namespace HahaHub.Services.Feed.Services.Abstractions
{
    public interface ICategoryService
    {
        Task<CategoryDto> GetByIdAsync(int id, CancellationToken cancellationToken);

        Task<PaginatedList<CategoryDto>> GetPaginatedListAsync(int pageNumber, int pageSize, CancellationToken cancellationToken);

        Task<int> AddAsync(CreatingCategoryDto categoryDto, CancellationToken cancellationToken);

        Task DeleteAsync(int id, CancellationToken cancellationToken);
    }
}
