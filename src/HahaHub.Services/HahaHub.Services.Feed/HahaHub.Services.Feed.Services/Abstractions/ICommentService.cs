﻿using HahaHub.Services.Feed.Services.Contracts;
using HahaHub.Services.Feed.Services.Contracts.Comments;
using System.Threading;
using System.Threading.Tasks;

namespace HahaHub.Services.Feed.Services.Abstractions
{
    public interface ICommentService
    {
        Task<int> AddAsync(CreatingCommentDto commentDto, CancellationToken cancellationToken);

        Task UpdateAsync(int id, UpdatingCommentDto commentDto, CancellationToken cancellationToken);

        Task DeleteAsync(int id,  CancellationToken cancellationToken);

        Task<CommentDto> GetByIdAsync(int id, CancellationToken cancellationToken);

        Task<PaginatedList<CommentDto>> GetPaginatedListByPostIdAsync(int postId, int pageNumber, int pageSize, CancellationToken cancellationToken);
    }
}
