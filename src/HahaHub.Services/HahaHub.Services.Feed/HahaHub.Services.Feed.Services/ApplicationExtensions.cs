﻿using HahaHub.Services.Feed.Services.Abstractions;
using HahaHub.Services.Feed.Services.Implementations;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace HahaHub.Services.Feed.Services
{
    public static class ApplicationExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services
                .AddTransient<IPostService, PostService>()
                .AddTransient<ICategoryService, CategoryService>()
                .AddTransient<ILikeService, LikeService>()
                .AddTransient<ICommentService, CommentService>();

            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            return services;
        }
    }
}
